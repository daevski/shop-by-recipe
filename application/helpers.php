<?php

// custom helpers
function ingrsList(){
	$ingrs = array();
		$ingredients = Ingredient::order_by('name')->get();
		foreach ($ingredients as $key => $i) {
			$ingrs[$key]['id'] = $i->ingr_id;
			$ingrs[$key]['name'] = $i->name;
		}
		return $ingrs;
}

// Simple decimals to fractions
function simpleD2F($num){

	$pos = strpos($num, '.');
	if( $pos != ''){
		$arr = explode('.', $num);
		$whole = substr($num, 0, $pos);
		$dec = substr($num, $pos);

		switch ($dec) {
			case '.5':
				$frac = '<sup>1</sup>&frasl;<sub>2</sub>';
				break;
			case '.25':
				$frac = '<sup>1</sup>&frasl;<sub>4</sub>';
				break;
			case '.75':
				$frac = '<sup>3</sup>&frasl;<sub>4</sub>';
				break;
			case '.333':
				$frac = '<sup>1</sup>&frasl;<sub>3</sub>';
				break;
			case '.666':
				$frac = '<sup>2</sup>&frasl;<sub>3</sub>';
				break;
			case '.125':
				$frac = '<sup>1</sup>&frasl;<sub>8</sub>';
				break;
			
			default:
				$frac = $dec;
				break;
		}

		echo ($whole!=0?$whole:'').'<span class="frac">'.$frac.'</span>';
	} else {
		echo $num;
	}
}