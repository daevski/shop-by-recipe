<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
*/

Route::get('/', 					array('as'=>'home', 'uses'=>'home@index'));
Route::get('/login',				array('as'=>'login', 'uses'=>'home@login'));
Route::get('/invite',				array('as'=>'login', 'uses'=>'home@invite'));
Route::get('/about-this-app',		array('as'=>'about', 'uses'=>'home@about'));
Route::get('/logout',				array('as'=>'logout', 'uses'=>'home@logout'));

Route::post('auth-login',			array('uses' => 'home@auth_login'));

/* The following require Auth */
Route::get('add/recipe', 				array('before' => 'auth', 'as'=>'addrecipe', 'uses'=>'add@recipe'));
Route::post('add/query-ingredient', 	array('before' => 'auth', 'uses' =>'add@query_ingredient'));
Route::post('add/submit-recipe', 		array('before' => 'auth', 'uses' =>'add@submit_recipe'));
Route::get('list/results', 				array('before' => 'auth', 'as'=>'list', 'uses'=>'list@list'));
Route::get('choose/recipes', 			array('before' => 'auth', 'as'=>'choose', 'uses'=>'list@choose'));
Route::get('view/recipe/(:num)', 		array('before' => 'auth', 'as'=>'viewrecipe', 'uses'=>'recipes@view_recipe'));
Route::get('recipes/all', 				array('before' => 'auth', 'as'=>'allrecipes', 'uses'=>'recipes@all'));
Route::post('recipes/submit-choices',	array('before' => 'auth', 'uses' =>'list@submit_choices'));
Route::post('recipes/(:num)/delete',	array('before' => 'auth', 'uses'=>'recipes@delete_recipe'));
Route::post('modify/list', 				array('before' => 'auth', 'uses' =>'list@modify_list'));

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('404', function()
{
	return Response::error('404');
});

Event::listen('500', function()
{
	return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Router::register('GET /', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function()
{
	// Do stuff before every request to your application...
});

Route::filter('after', function($response)
{
	// Do stuff after every request to your application...
});

Route::filter('csrf', function()
{
	if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::to('login');
});