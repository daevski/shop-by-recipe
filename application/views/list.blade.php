@layout('layouts.default')
@section('content')

<h3 class="small-centered small-11 medium-5 large-5 columns">Shopping List</h3>

@if ( Session::has('list') )
	<?php 
		$list = Session::get('list');
		// $recipes = Session::get('recipes');
	?>
	<table id="list" class="small-11 medium-5 large-5 medium-centered columns">
		<th>Amount</th>
		<th>Ingredient</th>
	@foreach($list as $l)
		<tr class="ingredient">
			<td class="amt text-center">{{ simpleD2F( $l['amt'] ).'<br>'. $l['unit'] }}</td>
			<td class="ingr">{{ $l['ingr'] }}</td>
		</tr>
	@endforeach
	</table>
@else 
	You can generate a shopping list from the {{ HTML::link_to_route('choose', 'Choose') }} page.
@endif

@endsection