@layout('layouts.default')
@section('content')
<div class="text-center small-10 medium-10 large-8 small-centered columns">
	<p class="panel callout radius large-6 small-centered columns">ShopByRecipe is an online app that will help you <b>store and maintain recipes</b> and <b>compile shopping lists</b> from them.</p>
	<h2><small>Features</small></h2> 
	<div class="panel radius"><strong>Store Recipes</strong> - Add recipes you love, recipes you make often, recipes you don't want to lose... we're working on making the site a full recipe manager, as well las a shopping tool.</div>
	<div class="panel radius"><strong>Make Shopping Lists</strong> - Select which recipes you want to make this week and hit the submit button. The app will return a list of all ingredients needed; if you have Milk listed for multiple recipes, it combines all the measurements and lists the total amount of Milk you will need to make all the recipes selected.</div>
	<p>Take the recipes you love to make, or new recipes you want to try, and with ShopByRecipe you can calculate exatly how much of each ingredient you'll need to make them all with one trip to the store.</p>
	
	<p>ShopByRecipe is for anyone that cooks.</p>
</div>
@endsection