@layout('layouts.default')
@section('content')

@if ( Session::has('msg') )
	<div class="text-center alert-box success" data-alert data-alert-fadeout>{{ Session::get('msg') }} 
		<a href="#" class="close">x</a></div>
@endif
	
	<h6 id="page-title">Add a new recipe...</h6>

	<br />

	<form id="addrecipe-form" class="small-12 medium-10 large-8 medium-centered large-centered columns" data-abide="ajax">
		<div id="ingrs" data-array='<?php echo json_encode($ingrs);?>'></div>
		<h5 class="section-header">Recipe Info</h5>
		<hr class="section-header-base">
		<div class="row large-11">
			<div class="small-12 medium-7 large-6 columns" data-joyride>
				<label>
					<input type="text" id="title" placeholder="Recipe Title" required pattern="recipe_title">
					<div class="moving-label"></div>
				</label><small class="error">Can't be blank; must be less than 48 characters.</small>
			</div>
			
		</div>
		<div class="row large-11">
			<div class="small-12 medium-5 large-4 columns">
				<select id="category" required>
					<option value="" disabled selected>No Category</option>
					@foreach($categories as $cate)
				    <option value="{{ $cate->id }}">{{ $cate->category }}</option>
				    @endforeach
				</select>
			</div>
			<div class="small-12 medium-2 large-2 columns end">
				<label>
					<input type="text" id="servings" class="text-center" placeholder="Servings" required pattern="number">
					<div class="moving-label"></div>
				</label><small class="error">Must be a number.</small>
			</div>
		</div>
		<h5 class="section-header">List of ingredients</h5>
		<hr class="section-header-base">
		<div id="list-of-ingredients" class="row">
			<div id="ingredients">
				<div class="ingredient small-12 medium-12 large-12 columns">
					<div class="text-center small-12 medium-2 large-1 columns">
						<label>
							<input type="text" class="amt inline" name="amount" placeholder="0" required pattern="number">
							<div class="moving-label"></div>
						</label><small class="error">^</small>
					</div>
					<div class="small-12 medium-3 large-2 columns" data-under-label="+ unit">
						<select name="units" id="units" class="inline">
							@foreach($units as $u)
							@if($u->unit == 'cup')
								<option value="{{ $u->abbr }}" selected="selected">{{ $u->unit }}</option>
							@else
								<option value="{{ $u->abbr }}">{{ $u->unit }}</option>
							@endif
						    @endforeach
						</select>
					</div>
					<div class="of-text text-center large-1 columns">
						<span>of</span>
					</div>
					<div class="small-12 medium-5 large-5 columns">
						<label>
							<input type="text" class="name" name="name" placeholder="Ingredient" required>
						</label><small class="error">Required.</small>
					</div>
					<div class="add-button small-1 medium-1 large-1 columns end">
						<input type="button" id="add-ingr" class="button success" value="&#43">
					</div>
				</div>
				<hr class="show-for-small-only">
			</div>
		</div>

		<h5 class="section-header">Instructions</h5>
		<hr class="section-header-base">
		<div id="list-of-instructions">
			<div id="instructions">
				<div class="instruction row small-12 medium-12 large-12 columns">
					<div class="text-center small-1 medium-1 large-1 columns">
						<div class="instruction-number">1</div><div class="num-dot">(dot)</div>
					</div>
					<div class="small-10 medium-9 large-8 columns end">
						<div name="instruction-text" class="instruction-text" contenteditable>
					</div>
				</div>
			</div>
		</div>



		<div class="add-recipe row text-center">
			<input type="submit" id="submit-recipe" class="button" name="submit-recipe" value="Add Recipe">
		</div>
	</form>
				<!-- <div id="convert">Need to convert?</div> -->


		<br>
		<hr>
		<h2 class="text-center large-4 medium-centered large-centered columns"><small>Shortcuts</small></h2>
		<div class="text-center small-12 medium-6 large-4 small-centered medium-centered large-centered callout panel columns">
			<p>Press <kbd>&#43</kbd> key to insert another ingredient.<br>and <kbd>&#45</kbd> to remove the current ingredient.</p>
		</div>


		<!-- START :: JOYRIDE STEPS -->
		
		<ol class="joyride-list" data-joyride>
			<li data-id="add-recipe" data-text="" data-options="tip_location: top; next_button: false; prev_button: false">
				<span>Start with a title</span>
			</li>
		</ol>

		<!-- <script type="text/javascript" id="WolframAlphaScript70ac430cec523000b780751af657aca1" src="http://www.wolframalpha.com/widget/widget.jsp?id=70ac430cec523000b780751af657aca1&output=lightbox&width=660"></script> -->
@endsection
