@layout('layouts.default')
@section('content')

{{ Form::open('auth-login', 'POST', array('class' => 'login box row small-7 medium-4 large-4 large-centered')) }}
	<h3>Login</h3>
	<p>Access ot this site is by <a href="invite">invite</a> only.</p>

	{{ Form::input('text', 'username', '', array('placeholder'=>'Username', 'class'=>'')) }}
	{{ Form::input('password', 'password', '', array('placeholder'=>'Password', 'class'=>'large-8 large-centered columns')) }}

	<div class="login row text-center">
		{{ Form::input('submit', 'submit-login', 'Login', array('class'=>'button')) }}
	</div>

{{ Form::close() }}

@endsection
