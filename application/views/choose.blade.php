@layout('layouts.default')
@section('content')
		<div id="choose">
			@foreach($recipes as $recipe)
			<div class="recipe small-11 medium-6 large-4 medium-centered columns">
				<div class="title row" recipe-id="{{ $recipe->recipe_id }}">
					<span name="title">{{ $recipe->title }}</span>
				</div>
				<div class="options row">
					<div class="switch small-2 medium-2 large-2 columns">
						<input id="{{ $recipe->recipe_id }}-checkbox" type="checkbox" name="selection" data-choose-recipe-id="{{ $recipe->recipe_id }}">
						<label for="{{ $recipe->recipe_id }}-checkbox"></label>
					</div>
					<div class="small-2 medium-2 large-2 columns">
						<input type="text" class="serving" name="serving" value="{{$recipe->servings}}" data-default-serving="{{$recipe->servings}}" disabled="disabled">
					</div>
					<div class="small-2 medium-2 large-2 columns end">
						<label for="serving" class="right inline">Servings</label>
					</div>
				</div>
			</div>
			@endforeach
			<div class="add-recipe row text-center">
				<input type="button" class="button" id="choose" name="choose" value="Create List">
			</div>
		</div>
@endsection