<!DOCTYPE HTML>
<html class="no-js" lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>ShopByRecipe | {{ $title }}</title>
	{{Asset::styles()}}
	{{Asset::scripts()}}
</head>
<body>

	<!-- START :: HEADER -->
	<div id="logo" class="large-12">
		<h1 id="site-name">ShopByRecipe</h1>
		<p id="tag-line" class="small-8 small-centered columns">Your recipes and shopping lists... As easy as pie.</p>
	</div>
	<!-- END :: HEADER -->


	<!-- START :: NAVIGATION -->
	<nav role="navigation">
		
	<?php if (Auth::check() == 'true'): ?>
		<div class="icon-bar four-up large-12">
			<a href="{{ URL::to_route('addrecipe') }}" class="item"><img src="{{ URL::base() }}/img/icon/Plus.svg" alt="icon">
				<label>Add Recipe</label>
			</a>
			<a href="{{ URL::to_route('choose') }}" class="item"><img src="{{ URL::base() }}/img/icon/List.svg" alt="icon">
				<label>Make Shopping List</label>
			</a>
			<a href="{{ URL::to_route('allrecipes') }}" class="item"><img src="{{ URL::base() }}/img/icon/MyRecipes.svg" alt="icon">
				<label>View My Recipes</label>
			</a>
			<a href="{{ URL::to_route('logout') }}" class="item"><img src="{{ URL::base() }}/img/icon/Logout.svg" alt="icon">
				<label>Logout</label>
			</a>
		</div>
	<?php else : ?>
		<div class="icon-bar three-up large-12">
			<a href="{{ URL::to_route('home') }}" class="item"><img src="{{ URL::base() }}/img/icon/Home.svg" alt="icon">
				<label>Home</label>
			</a>
			<a href="{{ URL::to_route('about') }}" class="item"><img src="{{ URL::base() }}/img/icon/About.svg" alt="icon">
				<label>About</label>
			</a>
			<a href="{{ URL::to_route('login') }}" class="item"><img src="{{ URL::base() }}/img/icon/Login.svg" alt="icon">
				<label>Login</label>
			</a>
		</div>
	<?php endif; ?>
	</nav>
	<!-- END :: NAVIGATION -->


	<!-- START :: FLASH MESSAGE  -->
	@if(Session::has('message'))
	<p class="message">{{ Session::get('message') }}</p>
	@endif
	<!-- END :: FLASH MESSAGE  -->


	<!-- START :: LAYOUT CONTENTS -->
	@yield('content')
	<!-- END :: LAYOUT CONTENTS -->


	<script>
      $(document).foundation();
      // $(document).foundation('joyride', 'start');
    </script>
</body>
</html>