@layout('layouts.default')
@section('content')

	<div class="about small-11 medium-6 large-6 medium-centered columns text-center">
		<h3>About this app</h3>
		<p>This app is intended as a tool for managing personal recipes and being able to generate content based on those recipes. It was started in 2012 as a side project and has been slowly growning ever since. Any questions, comments or concerns related to this app can be directed to:  <address><small><a href="mailto:david@daev.co">David McKee</a> &lt; david@daev.co &gt; <br> <span class="muted">This is a product built and hosted by DAEVCO</span></small></address></p>
	</div>
@endsection