@layout('layouts.default')
@section('content')

@if ( Session::has('success-msg') )
	<div class="text-center alert-box success" data-alert data-alert-fadeout>{{ Session::get('success-msg') }} 
		<a href="#" class="close">x</a></div>
@endif

<div id="recipe-cards" class="small-12 medium-12 large-10 small-centered column">
	<ul class="card-grid small-block-grid-1 medium-block-grid-2 large-block-grid-3">
	@foreach($recipes as $recipe)
		<li class="outter-card">
			<div class="card">
				<span class="recipe-id">{{ $recipe->recipe_id }}</span>
				<span class="category-tag"><small>{{ $recipe->category }}</small></span>
				<h5 class="truncate" title="{{ $recipe->title }}">{{ $recipe->title }}</h5> 
				<p>Description of recipe, possibly notes, too.</p>
				<p>
					{{  HTML::link_to_route('viewrecipe', 'View Recipe', $recipe->recipe_id, array('class' => 'radius label')) }} &nbsp;&nbsp;
					<span class="delete-recipe radius alert label">Delete Recipe</span>
				</p>
			 </div>
		</li>
	@endforeach
	</ul>
</div>


@endsection