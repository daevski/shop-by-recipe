@layout('layouts.default')
@section('content')

	<div class="about small-11 medium-6 large-6 medium-centered columns text-center">
		<h3>Invites</h3>
		<p>This app is currently in development, and not ready for the public. However, if you are still interested, please send an email to: <a href="mailto:david@daev.co">David [at] daev.co</a></p>
	</div>
@endsection