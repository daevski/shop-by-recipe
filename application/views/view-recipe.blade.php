@layout('layouts.default')
@section('content')
<table id="recipe-header">
	<th>{{ $title }}</th>
	<tr>
		<td>Servings: {{ $servings }}</td>
	</tr>
</table>

<table>
	<th>Amount</th>
	<th>Ingredient</th>
	@foreach($recipes as $recipe)
	<tr>
		<td class="amt">{{ simpleD2F( round($recipe->amount, 3) ) .'<br>'.$recipe->unit }}</td>
		<td class="ingr">{{ $recipe->name }}</td>
	</tr>
	@endforeach
</table>

@endsection