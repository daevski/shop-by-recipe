<?php 

class Recipes_Controller extends Base_Controller {
	public $restful = true;

	public function get_view_recipe($recipe_id){
		$id          = intval($recipe_id);
		$recipe_info = Recipe::where('recipe_id', '=', $id)->first();
		$recipes     = DB::table('recipe_ingr')
							->join('recipes', function($join){
								$join->on('recipe_ingr.recipe_id', '=', 'recipes.recipe_id');
							})
							->join('ingredients', function($join){
								$join->on('recipe_ingr.ingr_id', '=', 'ingredients.ingr_id');
							})
							->join('units', function($join){
								$join->on('recipe_ingr.unit_id', '=', 'units.id');
							})
							->where('recipes.recipe_id', '=', $id)
							->get(array('recipe_ingr.recipe_id', 'recipe_ingr.amount', 'recipes.title', 'recipes.servings', 'ingredients.name', 'units.unit'));

		return View::make('view-recipe')
				->with('id', $recipe_info->id)
				->with('title', $recipe_info->title)
				->with('servings', $recipe_info->servings)
				->with('recipes', $recipes);
	}

	public function get_all(){
		$recipes = DB::table('recipes')
					->join('categories', 'recipes.category_id', '=', 'categories.id')
					->order_by('recipes.title', 'asc')
					->get(array('recipes.recipe_id', 'recipes.title', 'recipes.servings', 'categories.category'));

		return View::make('all')
				->with('title', 'All Recipes')
				->with('recipes', $recipes);
	}

	public function post_delete_recipe($recipe_id){
		$id                   = intval($recipe_id);
		$affected_recipes     = DB::table('recipes')->where('recipe_id', '=', $id)->delete();
		$affected_recipe_ingr = DB::table('recipe_ingr')->where('recipe_id', '=', $id)->delete();

		// NEED an if statement here to determine success or failure.
		// and two different Flash messages.
		Session::flash('success-msg', 'Deleted Successfully: '.$affected_recipes);
		return Response::json(array('error'=>'0', 'url'=>URL::to_route('allrecipes')));
	}
}