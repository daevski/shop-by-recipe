<?php

class List_Controller extends Base_Controller {
	public $restful = true;

	public function get_choose(){
		return View::make('choose')
			->with('title', 'Choose Your Recipes!')
			->with('recipes', Recipe::order_by('title')->get());
	}
	
	public function get_list(){
		return View::make('list')
			->with('title', 'Your Shopping List!');
	}

	public function post_submit_choices(){
		$x = Input::json();
		$iLen = count($x);
		$ingrs = array();
		$allTitles = array();
		
		foreach($x as $id){
			// $allTitles = Recipe::where('recipe_id', '=', $id); 
			$amounts = DB::table('recipe_ingr')
							->join('ingredients', 'recipe_ingr.ingr_id', '=', 'ingredients.ingr_id')
							->join('units', 'recipe_ingr.unit_id', '=', 'units.id')
							->where('recipe_ingr.recipe_id', '=', $id)
							->get(array('recipe_ingr.amount', 'ingredients.ingr_id', 'ingredients.name', 'units.unit'));

			foreach($amounts as $ingredient) { 
				$ingr_id = $ingredient->ingr_id;
				unset($currAmt);

				// check for ingr already in ingrs array
				// Eventually will need wo add: && ($ingredient->unit == $ingrs[$ingr_id]['unit'])
				if( (array_key_exists($ingr_id, $ingrs)) ){
					$currAmt = intVal($ingrs[$ingr_id]['amt']);
					$ingrs[$ingr_id]['amt'] = round(($ingredient->amount + $currAmt), 3);
				} else { $ingrs[$ingr_id]['amt'] = round($ingredient->amount, 3); }

				$ingrs[$ingr_id]['ingr'] = $ingredient->name;
				$ingrs[$ingr_id]['unit'] = $ingredient->unit;
			}
		}

		// Store $ingrs in the session for the next request from the user
		Session::flash('list', $ingrs);
		// Session::flash('recipes', $allTitles);

		// Return a JSON object containing the URL to redirect to
		return Response::json(array('error' => 0, 'url' => URL::to_route('list')));
	}
}