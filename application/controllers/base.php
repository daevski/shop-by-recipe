<?php

class Base_Controller extends Controller {

	public function __construct()
	{
	    //Assets
	    Asset::add('foundation-css', 'css/foundation.css');
	    Asset::add('custom-css', 'css/custom.css');
	    Asset::add('jquery-2.1.1', 'js/vendor/jquery.js');
	    Asset::add('modernizr', 'js/vendor/modernizr.js');
	    Asset::add('foundation-js', 'js/foundation.min.js');
	    Asset::add('foundation-abide', 'js/foundation/foundation.abide.js');
	    Asset::add('foundation-joyride', 'js/foundation/foundation.joyride.js');
	    Asset::add('foundation-jqcookie', 'js/vendor/jquery.cookie.js');
	    Asset::add('javascript', 'js/functions.js');
	    parent::__construct();
	}

	/**
	 * Catch-all method for requests that can't be matched.
	 *
	 * @param  string    $method
	 * @param  array     $parameters
	 * @return Response
	 */
	public function __call($method, $parameters)
	{
		return Response::error('404');
	}

}