<?php 

class Home_Controller extends Base_Controller {
	public $restful = true;
	
	public function get_index(){
		return View::make('index')
			->with('title', 'The Shopping Calulator!');
	}

	public function get_about(){
		return View::make('about')
			->with('title', 'About this app');
	}

	public function get_login(){
		return View::make('login')
			->with('title', 'Login');
	}

	public function get_invite(){
		return View::make('invite')
			->with('title', 'Invites');
	}

	public function post_auth_login(){
		$userdata = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);
		if(Auth::attempt($userdata))
		{
			return Redirect::to_route('addrecipe');
		}
		else 
		{
			return Redirect::to_route('login')
				->with('message', 'Username or password were not correct.');
		}
	}

	public function get_logout(){
		Auth::logout();
		return View::make('index')
			->with('title', 'The Shopping Calulator!');
	}
}