<?php 

class Add_Controller extends Base_Controller {
	public $restful = true;
	
	public function get_recipe(){
        return View::make('add')
        	->with('title', 'Add A Recipe')
        	->with('units', Unit::order_by('unit')->get())
        	->with('categories', Category::order_by('category')->get())
        	->with('ingrs', ingrsList());

	}

	public function post_query_ingredient(){
		$text = Input::json();
		$results = DB::table('ingredients')
					->where('name', 'LIKE', "%$text%")
					->get();
		return Response::json($results);
	}

	public function post_submit_recipe(){
		$i = Input::json();
		$title = $i->title;
		$categ = $i->category;
		$servs = $i->servings;

	// create entry for recipe
		Recipe::create(array(
			'title'=>$title,
			'category_id'=>$categ,
			'servings'=>$servs ));

	// find recipe_id from recipes table
		$recipeGet = Recipe::where('title', '=', $title )->first();
		$recipe_id = $recipeGet->recipe_id;

	// get the db ingredient list for comparison
		$dbIngrs = Ingredient::order_by('name')->get();
		$dbIngrArr = array();
		foreach($dbIngrs as $dbi){
			$id = $dbi->ingr_id;
			$name = $dbi->name;

			$dbIngrArr[$id] = $name;
		}

	// interpret the submitted ingredients
		$index = 0;
		$ingrString = null;
		$ingrsAll = array();
		foreach($i->ingredients as $ingredient){
			foreach($ingredient as $k => $v){
				$modu = $index % 3; 
				// modu is 0, 1, 2
				// $k = Ingredient, Amount, Unit
				// $v = Milk, 4, cups

				switch ($modu) {
					case '0':
						$ingr = $v;
						$ingrsAll .= $v; 
					// if ingr is not in db, add it
						if( !in_array($v, $dbIngrArr) ){
							Ingredient::create(array(
								'name'=>$v ));
						}
						break;
					case '1':
						$amt = $v;
						break;
					case '2':
						$unit = $v;
						break;
				}
				$index++;
			}

			// fetch ingr and unit IDs
			$unitLast = substr($unit, -1);
			$unitLen = strlen($unit);
			if($unitLast == 's'){
				$unit = substr($unit, 0, $unitLen-1);
			}

			$ingrGet = Ingredient::where('name', '=', $ingr)->first();
			$unitGet  = Unit::where('unit', '=', $unit)->first();
			$ingr_id = $ingrGet->ingr_id;
			$unit_id = $unitGet->id;

	// create recipe_ingr entry
		RecipeIngr::create(array(
				'recipe_id'=>$recipe_id,
				'ingr_id'=>$ingr_id,
				'amount'=>$amt,
				'unit_id'=>$unit_id
			));
		}

		Session::flash('msg', 'Added Successfully');
		return Response::json(array('error'=>'0', 'url'=>URL::to_route('addrecipe')));
	}
}