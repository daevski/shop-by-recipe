TODO:

[Show input / Add another]
	"Preparation" (Chopped, sliced, halved)
		anchor-link somewhere inline with ingredient row creates input
			store in db
		Needs keyboard shortcut "/+P"

[Show input / Add another]
	"Condition" (Fresh, pickled, spoiled)
		anchor-link somewhere inline with ingredient row creates input
			store in db
		Needs keyboard shortcut "/+C"


Add another
	"Unit"
		little icon in top-right corner
		popup modal with input
			add to db
			js add to select options
				sort options alphabetically

Hover "View Recipe" should show the recipe in popup near the cursor

Convert to Ajax "Delete Recipe"
	then hide with jquery fadeOut.