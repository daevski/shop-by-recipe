
// query Ingrdients
function queryIngr(ingredients) {
	var input       = $('input.name:focus'); // currently focused
	var value       = input.val();
	var ingrs       = $.parseJSON(ingredients);
	var resultsList = $('#results').children('ul');

	// clear the results list
	resultsList.html('');

	if(value != ''){
		resultsList.show()
	}
	for(var i=0; i<= ingrs.length -1; i++){
		if(ingrs[i].name.toLowerCase().indexOf(value.toLowerCase()) >= 0){
			resultsList.append(
				'<li>'
				+'<span class="id">'+ingrs[i].id+'</span>'
				+'<span class="name">'+ingrs[i].name+'</span>'
				+'</li>')
		}
	}
}


function moveLabel(inputElement) {
	/* This moving label function will take input attribute "placeholder" value and apply
	it to a div outside of the input, so make up for placeholder text not being seen 
	after user has entered text. If "placeholder" if not desired, additional "label" attribute 
	and that will be used first. 
	*/

// if the input is empty, the placeholder will be seen and moving label hidden
	if ( inputElement.val() == '' ){
		inputElement.next('.moving-label').fadeOut(1000);
	}
// otherwise the Label, or Placeholder value will be used and moving label will be seen
	else {
		if ( inputElement.is("[label]") ) { 
			var inputText = inputElement.attr('label'); } 
		else {
			var inputText = inputElement.attr('placeholder');}

		var movingLabel = inputElement.next('.moving-label')

		movingLabel.text(inputText).fadeIn(1000)
	}
}



	/* ----------------------------------------- */

$(document).ready(function(){

	/* 
	-----------------------------------------
	|			Add Recipe
	-----------------------------------------
	*/

	var ingrs = $('#ingrs').attr('data-array')

// Moving Labels (to show input label when input is focused)
	$('#addrecipe-form input').on('keyup', function(e){ moveLabel($(this)) })
	$('#addrecipe-form input').on('blur').next('.moving-label').css('display', 'none')


// query and select ingridients
	if($('form#addrecipe-form').length>0){ // only on add recipe page
		//create keyTimer
		var keyTimer;
		//focus recipe title on load
		$('input#title').trigger('focus');

	// #results keyboard functionality
		$(document).on('keydown','input.name:focus', function(e){
			var $this        = $(this)
			var hovered      = $('li.hovered')
			var results      = $this.closest('.ingredient').find('input').siblings('#results')

			var lastListItem = results.find('li').last()
			var firstListItem = results.find('li').first()

	        clearTimeout(keyTimer);

	        if ( $this.val().length > 0 ) {
	        	results.show()
		        switch(e.which)
		        {
		            case 40: // down arrow
		                if( (hovered.length == 0) || ( $(hovered).is(lastListItem) ) ){
		                	hovered.removeClass("hovered")
		                    results.find('li').eq(0).addClass("hovered");
		                } else {
		                    hovered.removeClass("hovered").next().addClass("hovered");
		                }
		                break;
		            case 38: // up arrow
		                if( (hovered.length == 0) || ( $(hovered).is(firstListItem) ) ){
		                    hovered.removeClass("hovered")
		                    results.find('li').last().addClass("hovered");
		                } else {
		                    hovered.removeClass("hovered").prev().addClass("hovered");
		                }
		                break;
	                case 9:
	                case 13: // enter (13) or tab (9) key
	                	// if no result list items is selected, break out.
	                	if( $('li.hovered').length == 0 ) { break; }
	                	// select result list item and enter into input
	                	results.hide()
	                	$this.val( $('.hovered').children('span.name').html() );
	                	$this.closest('div').next('div').find('input').trigger('focus');
	                	return false;
	                	break;
		            default: // any key
		                keyTimer = setTimeout(function(){
						queryIngr(ingrs)}, 200);
		                break;
		        }
		    }
		})
		// prepending and removing results on focus/blur
		$(document).on('focus', 'input.name', function(){
			$(this).before('<div id="results"><ul class="result-list"></ul></div>');
		});
		$(document).on('blur', 'input.name', function(){
			$('#results').remove()
		});

	// #results mouse functionality
		// on mouse hover, make THIS .hovered
		$(document).on('hover', 'ul.result-list li', function(){
			$('li.hovered').removeClass('hovered');
			$(this).addClass('hovered');
		});
		// on mouse click, use in input
		$(document).on('click','ul.result-list li', function(){
			// select list item and insert into input
			var $this      = $('.hovered')
			var parentIngr = $this.closest('div.ingredient')
			var inputName  = parentIngr.find('input.name')

			inputName.val( $this.children('span.name').html() )
			$('#results').remove()
		});


	// add or remove ingredient
		$(document).on('click', '#add-ingr', function(){
			var $this         = $(this);
			var thisIngr      = $this.closest('.ingredient');
			thisIngr.clone().insertAfter(thisIngr),
			$this.val('-')
			// clear and reset input values
			$('.ingredient').last().find('input:not(:button)').each(function(index){ 
				$(this).val('') 
			})
			// change plus button to minus button 
			$this.removeAttr('id').removeClass('success').addClass('alert').addClass('subtr-ingr'),
			// focus new ingredient first input
			$('.ingredient').last().find('input').eq(0).focus()
		})
		$(document).on('click', 'input.subtr-ingr', function(){
			// only remove this ingredient if it is not the only ingredient
			// THIS CODE IS DUPLICATED AT KEYUP EVENT
			var thisIngr = $(this).closest('.ingredient');
			var index = thisIngr.index();
			if( $('.ingredient').length!==1 ) {
				thisIngr.remove();
				$('.ingredient').eq(index).find('input.name').trigger('focus');
			}
		});

	
		// blur so that the ipnut does not receive the key
		$(document).on('keydown', 'input:focus', function(e){
			if( e.keyCode==107 ) {
				$('input:focus').trigger('blur');
			}
		});


		// keypress "+" and "-" sensing to add/remove ingredient
		$(document).on('keyup', function(e){
			switch(e.keyCode){
				case 107:
					$('#add-ingr')
						.delay(400).trigger('click')
						$('#addrecipe-form').foundation('abide', 'reflow'); // updates Abide for validation
					break;
				case 109:
					var thisIngr = $( document.activeElement ).closest('.ingredient');
					// if this ingredient has a minus button, trigger click of that button
					// THIS CODE IS DUPLICATED AT CLICK EVENT
					if( thisIngr.find('.subtr-ingr').length >0 ){
						var index = thisIngr.index();
						if( $('.ingredient').length!==1 ) {
							thisIngr.remove();
							$('.ingredient').eq(index).find('input').eq(0).trigger('focus');
						}
					}
					// if this ingredient row does not have a minus button, 
					// aka is the last ingredient
					if( !thisIngr.find('.subtr-ingr').length >0 ) {
						// if this is the ONLY ingredient, do nothing.
						if ( $('.ingredient').length<2 ){
							break;
						}
						// if this is the last ingredient, change previous button to be "+"
						var index = thisIngr.index();
						var prevSubtrBtn = thisIngr.prev().find('input.subtr-ingr');
						prevSubtrBtn.val('+');
						prevSubtrBtn.attr('id', 'add-ingr').removeClass('alert')
							.addClass('success').removeClass('subtr-ingr');
						thisIngr.remove();
						$('.ingredient').eq(index-1).find('input').eq(0).trigger('focus');
					}
					$('#addrecipe-form').foundation('abide', 'reflow'); // updates Abide for validation
					break;
				default:
					// nothing to happen here...
					break;

			}
		});




/* Submit Recipe */

		// If abide form validation fails, focus failed input
		$('#addrecipe-form').on('invalid.fndtn.abide', function(){
			$(this).find('[data-invalid]').focus()
		})

		// If abide form validation succeeds, gather info and submit
		$('#addrecipe-form').on('valid.fndtn.abide', function(){
				
			if ($('input#serving').val()==''){
				var serv = $('input#servings').attr('placeholder');
			} else {
				var serv = $('input#servings').val();
			}

			var title = $('input#title').val()
			var categ = $('select#category option:selected').val()
			var ingrLen = $('.ingredient').length
			var recipe = {
				"title": title,
				"category": categ,
				"servings": serv,
				"ingredients": {}
				}

			$('.ingredient').each(function(index){
				var $this = $(this)
				var ingr = $this.find('input.name').val()
				var amt = $this.find('.amt').val()
				var unit = $this.find('#units option:selected').html()
				recipe.ingredients[ingr] = 
					{
						"ingr": ingr,
						"amt": amt, 
						"unit": unit
					}
				if(ingrLen-1 == index){

				// send via ajax
					$.post('submit-recipe/', JSON.stringify(recipe),
						function(json){
							window.location.href = json.url
					})
				}
			})
		})

	// on Add Recipe page load, pluralize the appropriate units
		$('select#units option').each(function(){
			var $this = $(this)
			var thisHtml = $this.html()

			if( thisHtml != 'small' && thisHtml != 'large' && thisHtml != 'whole' && thisHtml != 'half' ){
				$this.html(thisHtml+'s')
			}

		})
	
	// unpluralize for '1' in the amount field before units
		$(this).on('keyup', 'input.amt', function(){
			if( $('#addrecipe-form').length > 0 ){
				var $this = $(this)
				var input = $this.val()
				var option = $this.closest('.ingredient').find('option')

				if( (input <= '1') && (input > '0') ){
					option.each(function(){
						var $this = $(this)
						var thisHtml = $this.html()
						var len = thisHtml.length
						var last = thisHtml.substr(-1, 1)

						if( thisHtml != 'small' && thisHtml != 'large' && thisHtml != 'whole' && thisHtml != 'half' ){
							if(last == 's'){
								$this.html( thisHtml.substr(0,len-1) )
							}
						}
					})
				} else {
					option.each(function(){
						var $this = $(this)
						var thisHtml = $this.html()
						var len = thisHtml.length
						var last = thisHtml.substr(-1, 1)

						if( thisHtml != 'small' && thisHtml != 'large' && thisHtml != 'whole' && thisHtml != 'half' ){
							if(last != 's'){
								$this.html( thisHtml+'s' )
							}
						}
					})
				}
			}
		})

	// show conversion table
		$(this).on('click', '#convert', function(){
			var table = $('#wolframAlphaWidget25600')
			// var state = table.css('visibility')

			// console.log(state)

			if( $('#wolframAlphaWidget25600').css('visibility') == 'hidden' ){
				table.css('visibility', 'visible').animate({ right: '-7%' }, 1500)
			} else {
				table.animate({ right: '-55%' }, 1500, function(){
					table.css('visibility', 'hidden')
				})
			}
		})

	} // END OF add recipe page check

	/* 
	-----------------------------------------
	|			Choose Recipes
	-----------------------------------------
	*/
	if($('div#choose').length>0){
		$(this).on('click', 'input#choose', function(){
			var $this = $(this)
			var selected = $('input[name="selection"]:checked')
			var list = []

			// compile list of IDs
			selected.each(function(){
				list.push($(this).data('choose-recipe-id'))
			})
			
			// ajax send 
			$.post('/recipes/submit-choices/', JSON.stringify(list),
				function(json){
					window.location.href = json.url;
				});
		})

	// visual aids for selections 
		$('input[type="checkbox"]').on('change', function(){
			var $this          = $(this);
			var recipe         = $this.closest('.recipe');
			var titleRow       = recipe.find('.title.row');
			var checkmark      = '<div class="checkmark small-1 medium-1 large-1 columns right">'
									+'<div class="checkmark-icon large-1 columns"></div>';
			var serving        = recipe.find('input[name="serving"]');
			var defaultServing = recipe.find('input[name="serving"]').attr('data-default-serving');

			switch ($this.is(':checked')) {
				case true:
					recipe.addClass('selected')
					titleRow.append(checkmark)
					serving.prop('disabled', false);
					serving.select()
					break;
				case false:
					recipe.removeClass('selected')
					titleRow.find('.checkmark').remove()
					serving.val(defaultServing)
					serving.prop('disabled', true);
					break;
			}

		})

		// modify list
	} // END OF choose page check


	/* 
	-----------------------------------------
	|			View All My Recipes
	-----------------------------------------
	*/
	if( $('#recipe-cards').length > 0) { // Check that use is on the All Recipes page


		/* Delete Recipe */
		$('.delete-recipe').on('click', function(){
			var recipe_id = $(this).closest('li').find('span.recipe-id').html();
			$.post(recipe_id+"/delete",
				function(json){
					window.location.href = json.url;
				});
		})





	}


	/* 
	-----------------------------------------
	|			Miscellaneous
	-----------------------------------------
	*/

	// Foundation 5 alert box with data-alert-fadeout
	var alertMsg = $('[data-alert-fadeout]').eq(0)
	alertMsg.delay(5000).queue(function(){
		alertMsg.children('a').trigger('click');
	});

	// Foundation Abide patterns
	$(document)
	  .foundation({
	    abide : {
	      patterns: {
	        recipe_title: /^[a-zA-Z0-9].{1,48}$/
	      }
	    }
	  });

	// click on under-label
	$('[data-under-label]').on('click', function(){
		console.log('Something amazing just happened.')
	})



}) /* end of Doc.Ready */
